package org.gameoflife.controller;

import org.gameoflife.service.generation.CalculateNextGeneration;
import org.gameoflife.service.parselife.ParseLifeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

@RestController
@RequestMapping("/api")
@CrossOrigin("Http://localhost:4200")
public class GameOfLifeController {

  @Autowired
  private ParseLifeFactory parseLifeFactory;

  @Autowired
  private CalculateNextGeneration calculateNextGeneration;

  @GetMapping(path = "/file-names")
  public ArrayList<String> getFileNames() {
    ArrayList<String> listOfFiles = new ArrayList<>();

    for (final File file : new File(ParseLifeFactory.LIFE_FILE_DIRECTORY).listFiles()) {
      listOfFiles.add(file.getName());
    }

    return listOfFiles;
  }

  @GetMapping(path = "/parse-file/{fileName}")
  public HashSet<String> parseFile(@PathVariable String fileName) throws Exception {
    if (fileName.isEmpty()) {
      throw new Exception("Invalid file name.");
    }

    return parseLifeFactory.getParseLife(fileName).parseFile();
  }

  @PostMapping(path = "/next-generation/{gridWidth}-{gridHeight}")
  public HashSet<String> calculateNextGeneration(
          @RequestBody HashSet<String> liveCells,
          @PathVariable(value="gridWidth") int gridWidth,
          @PathVariable(value="gridHeight") int gridHeight
  ) {
    return calculateNextGeneration.calculateGeneration(liveCells,gridWidth, gridHeight);
  }

}
