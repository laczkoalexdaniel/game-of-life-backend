package org.gameoflife.service.generation;

import java.util.HashSet;

public interface CalculateNextGenerationInterface {

  HashSet<String> calculateGeneration(HashSet<String> liveCells, int gridWidth, int gridHeight);

}
