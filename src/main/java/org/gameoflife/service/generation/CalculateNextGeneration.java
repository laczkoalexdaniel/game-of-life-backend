package org.gameoflife.service.generation;

import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class CalculateNextGeneration implements CalculateNextGenerationInterface {

  private HashSet<String> liveCells;
  private int halfGridWidth;
  private int halfGridHeight;

  /**
   * Returns possible neighbours.
   * @param coordinate
   * @return HashSet<String>
   */
  private HashSet<String> getPossibleNeighbours(String coordinate) {
    int x = Integer.parseInt(coordinate.split(" ")[0]);
    int y = Integer.parseInt(coordinate.split(" ")[1]);
    HashSet<String> possibleNeighbours = new HashSet<>();

    possibleNeighbours.add((x) + " "  + (y - 1));
    possibleNeighbours.add((x - 1) + " "  + (y - 1));
    possibleNeighbours.add((x - 1) + " "  + y);

    boolean underYThreshold = Math.abs(y) <= halfGridHeight;

    if (underYThreshold) {
      possibleNeighbours.add(x + " " + (y + 1));
      possibleNeighbours.add((x - 1) + " "  + (y + 1));
    }

    boolean underXThreshold = Math.abs(x) <= halfGridWidth;

    if (underXThreshold) {
      possibleNeighbours.add((x + 1) + " " + y);
      possibleNeighbours.add((x + 1) + " "  + (y - 1));
    }

    if (underXThreshold && underYThreshold) {
      possibleNeighbours.add((x + 1) + " "  + (y + 1));
    }

    return possibleNeighbours;
  }

  /**
   * Returns the number of live neighbours.
   * @param coordinate
   * @return int
   */
  private int getLiveNeighboursInNumber(String coordinate) {
    int countLiveNeighbours = 0;

    for (String neighbourCoordinate: getPossibleNeighbours(coordinate)) {
      if (liveCells.contains(neighbourCoordinate)) {
        countLiveNeighbours++;

        if (countLiveNeighbours == 4) {
          break;
        }
      }
    }

    return countLiveNeighbours;
  }

  /**
   * Returns if cell stay alive or not.
   * @param coordinate
   * @return boolean
   */
  private boolean processLiveCell(String coordinate) {
    int liveNeighbours = getLiveNeighboursInNumber(coordinate);

    return liveNeighbours == 2 || liveNeighbours == 3;
  }

  /**
   * Returns if cell comes alive.
   * @param coordinate
   * @return boolean
   */
  private boolean processDeadCell(String coordinate) {
    return getLiveNeighboursInNumber(coordinate) == 3;
  }

  /**
   * Processing given coordinate.
   * @param nextGeneration
   * @param coordinate
   */
  private void processCell(HashSet<String> nextGeneration, String coordinate) {
    if (processLiveCell(coordinate)) {
      nextGeneration.add(coordinate);
    }

    for (String neighbour: getPossibleNeighbours(coordinate)) {
      if (!liveCells.contains(neighbour)) {
        if (processDeadCell(neighbour)) {
          nextGeneration.add(neighbour);
        }
      }
    }
  }

  /**
   * Calculates next generation.
   * @param liveCells
   * @param gridWidth
   * @param gridHeight
   * @return
   */
  public HashSet<String> calculateGeneration(HashSet<String> liveCells, int gridWidth, int gridHeight) {
    this.liveCells = liveCells;
    this.halfGridWidth = gridWidth / 2;
    this.halfGridHeight = gridHeight / 2;

    HashSet<String> nextGeneration = new HashSet<>();

    for (String liveCell :liveCells) {
      processCell(nextGeneration, liveCell);
    }

    return nextGeneration;
  }

}
