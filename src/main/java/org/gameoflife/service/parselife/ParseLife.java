package org.gameoflife.service.parselife;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Scanner;

@Component
public abstract class ParseLife implements ParseLifeInterface {

  public abstract HashSet<String> parseFile();

  private HashSet<String> liveCellCoordinates;
  private Scanner scanner;

  public ParseLife() {
    this.liveCellCoordinates = new HashSet<>();
  }

  /**
   * Returns live cells.
   * @return HashSet<String>
   */
  public HashSet<String> getLiveCellCoordinates() {
    return liveCellCoordinates;
  }

  /**
   * Setting scanner.
   * @param scanner
   */
  public void setScanner(Scanner scanner) {
    this.scanner = scanner;
  };

  /**
   * Returns scanner.
   * @return
   */
  public Scanner getScanner() {
    return this.scanner;
  }

}
