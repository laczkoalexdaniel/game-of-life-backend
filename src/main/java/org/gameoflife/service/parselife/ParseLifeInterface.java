package org.gameoflife.service.parselife;

import java.util.HashSet;

public interface ParseLifeInterface {

  HashSet<String> parseFile();

}
