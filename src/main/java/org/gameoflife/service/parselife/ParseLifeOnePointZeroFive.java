package org.gameoflife.service.parselife;

import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class ParseLifeOnePointZeroFive extends ParseLife {

  static final char DEAD_CELL = '.';
  static final char LIVE_CELL = '*';
  static final int LINE_LENGTH = 80;

  private Integer topLeftXCoordinate;
  private Integer topLeftYCoordinate;
  private int lineCounterInBlock;

  public ParseLifeOnePointZeroFive() {
    super();
    this.topLeftXCoordinate = null;
    this.topLeftYCoordinate = null;
    this.lineCounterInBlock = 0;
  }

  /**
   * Returns coordinate as string.
   * @param lineXCoordinate
   * @return String
   */
  private String getCoordinateAsString(int lineXCoordinate) {
    return (topLeftXCoordinate + lineXCoordinate) + " " + (topLeftYCoordinate + lineCounterInBlock - 1);
  }

  /**
   * Setting live cells.
   * @param line
   */
  private void setLiveCells(String line) {
    for (int i = 0; i < line.length(); i++) {
      String coordinateAsString = getCoordinateAsString(i);

      if (line.charAt(i) == LIVE_CELL && !getLiveCellCoordinates().contains(coordinateAsString)) {
        getLiveCellCoordinates().add(coordinateAsString);
      }
      else if (line.charAt(i) == DEAD_CELL && getLiveCellCoordinates().contains(coordinateAsString)) {
        getLiveCellCoordinates().remove(coordinateAsString);
      }
    }
  }

  /**
   * Removes live cells if dead cell overrides it.
   * @param lineLengthWithoutTruncate
   */
  private void setDeadCells(int lineLengthWithoutTruncate) {
    for (int x = lineLengthWithoutTruncate; x < LINE_LENGTH; x++) {
      if (getLiveCellCoordinates().contains(getCoordinateAsString(x))) {
        getLiveCellCoordinates().remove(getCoordinateAsString(x));
      }
    }
  }

  /**
   * Setting cell by the actual line.
   * @param line
   */
  private void setCells(String line) {
    setLiveCells(line);
    setDeadCells(line.length());
  }


  /**
   * Returns live cells by given file.
   * @return HashSet<String>
   */
  @Override
  public HashSet<String> parseFile() {
    while (getScanner().hasNextLine()) {
      String line = getScanner().nextLine();

      if (line.length() >= 2 && line.substring(0, 2).equals("#P")) {
        topLeftXCoordinate = Integer.parseInt(line.split(" ")[1]);
        topLeftYCoordinate = Integer.parseInt(line.split(" ")[2]);
        lineCounterInBlock = 1;
        continue;
      }
      else if (!line.substring(0, 1).equals("#")) {
        setCells(line);
      }

      lineCounterInBlock++;
    }

    getScanner().close();

    return getLiveCellCoordinates();
  }

}
