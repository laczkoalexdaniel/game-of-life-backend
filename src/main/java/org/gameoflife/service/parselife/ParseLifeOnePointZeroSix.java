package org.gameoflife.service.parselife;

import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class ParseLifeOnePointZeroSix extends ParseLife {

  private HashSet<String> liveCellCoordinates;

  public ParseLifeOnePointZeroSix () {
    this.liveCellCoordinates = new HashSet<>();
  }

  /**
   * Returns live cells by given file.
   * @return HashSet<String>
   */
  @Override
  public HashSet<String> parseFile() {
    while (getScanner().hasNextLine()) {
      getLiveCellCoordinates().add(getScanner().nextLine());
    }

    getScanner().close();

    return getLiveCellCoordinates();
  }

}
