package org.gameoflife.service.parselife;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

@Service
public class ParseLifeFactory {

  public static final String LIFE_FILE_DIRECTORY = "src/main/resources/life_files/";

  /**
   * Returns the right class for file parsing.
   * @param fileName
   * @return ParseLife
   * @throws FileNotFoundException
   */
  public static ParseLife getParseLife(String fileName) throws FileNotFoundException {
    ParseLife parseLife = null;

    Scanner scanner = new Scanner(new File(LIFE_FILE_DIRECTORY + fileName));
    String line = scanner.nextLine();

    switch(line.split(" ")[1]) {
      case "1.05":
        parseLife = new ParseLifeOnePointZeroFive();
        break;
      case "1.06":
        parseLife = new ParseLifeOnePointZeroSix();
        break;
    }

    parseLife.setScanner(scanner);

    return parseLife;
  }

}
