A feladat a következő:

Game Of Life

    Tervezd meg és készítsd el a Game Of Life nevű játékot a legjobb tudásod szerint.
    A kiválasztott programnyelvek / eszközök használatát teljesen rád bízzuk.
    Tedd elérhetővé számunkra, hogy ki tudjuk próbálni.
    Küldd el az alkalmazás forráskódját (örülnénk egy GitHub / Gitlab / stb. repónak).

A játék leírása:

http://en.wikipedia.org/wiki/Conway's_Game_of_Life

http://www.conwaylife.com/wiki/Life_1.05
http://www.conwaylife.com/wiki/Life_1.06

Az alkalmazásnak a következőket kell tudnia:

    Be lehet tölteni mintákat, aminek a formátuma .lif
        A csatolt zip fájl-ban találhatsz pár mintát
    Az egyes generációkból ki tudja számolni a következőt
    A generációkat meg tudja jeleníteni grafikusan
    Van egy léptető gomb és egy automata lejátszó gomb
    Kijelzi, hogy hányadik generációnál járunk a kiinduló állapothoz képest
    Lehet visszafele is léptetni (opcionális)

Extra feladat (nem kötelező), minden pont opcionális; hiányukban nem jár értük "mínusz pont":

    Válaszd külön a backend-et és frontend-et

·         A Backendet Java, SpringBoot alapokon is megírhatod

·         A Frontendet, ha még nem vagy annyira jó benne, elég ha sima HTML-ben összekattintgatod, hogy legyen egy felülete is a kódnak és fusson.